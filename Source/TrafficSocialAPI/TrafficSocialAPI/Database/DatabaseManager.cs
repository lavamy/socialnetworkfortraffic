﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TrafficSocialAPI.Helpers;
using TrafficSocialAPI.Models;

namespace TrafficSocialAPI.Database
{
    public class DatabaseManager
    {
        private static DatabaseManager currentObject;

        public static DatabaseManager getInstance()
        {
            if (currentObject == null)
                currentObject = new DatabaseManager();
            return currentObject;
        }

        private TrafficSocialDBEntities DbEntities;

        private DatabaseManager()
        {
            DbEntities = new TrafficSocialDBEntities();
        }

        public async Task<AspNetUser> FindUser(string username)
        {
            try
            {
                AspNetUser userDb = await DbEntities.AspNetUsers.FirstOrDefaultAsync(u => u.UserName.Equals(username));
                return userDb;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> UpdateInfo(string username, UpdateInfoBindingModel model)
        {
            try
            {
                AspNetUser userDb = await FindUser(username);
                userDb.Name = model.name;
                userDb.PhoneNumber = model.phone_number;
                await DbEntities.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> PostImage(ApplicationUser user, Image image)
        {
            try
            {
                AspNetUser userDb = await DbEntities.AspNetUsers.FirstOrDefaultAsync(u => u.Id.Equals(user.Id));
                image.AspNetUser = userDb;
                image.PostDate = DateTime.Now;

                DbEntities.Images.Add(image);
                await DbEntities.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Image> GetPosts(DateTime fromDay, int count)
        {
            try
            {
                //IQueryable<Image> result = DbEntities.Images.Where(i => DateTime.Compare(i.PostDate, fromDay) < 0);
                //return await result.Skip(Math.Max(result.Count(), result.Count() - count)).ToListAsync();
                return DbEntities.Images.Where(i => DateTime.Compare(i.PostDate, fromDay) < 0).TakeLast(count).ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public async Task<List<Image>> GetPosts(DateTime fromDay)
        {
            try
            {
                return
                    await
                        DbEntities.Images.Where(i => DateTime.Compare(i.PostDate, fromDay) > 0).ToListAsync();
            }
            catch
            {
                return null;
            }
        }

        public async Task<Image> FindPost(int postID)
        {
            try
            {
                Image imageDb = await DbEntities.Images.FirstOrDefaultAsync(u => u.Id.Equals(postID));
                return imageDb;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> DeletePost(int postID)
        {
            try
            {
                DbEntities.Images.Remove(await FindPost(postID));
                await DbEntities.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditPost(NewsFeedUpdateBindingModel model)
        {
            try
            {
                Image imageDb = await FindPost(model.post_id);
                imageDb.Description = model.description;
                imageDb.ErrorWarning = model.error_warning;
                imageDb.LocationDetail = model.location_detail;
                imageDb.Note = model.note;
                imageDb.Province = model.province;
                imageDb.Route = model.route;
                imageDb.Warning = model.warning;
                //imageDb.PostDate = DateTime.Now;

                if (model.location != "null")
                    imageDb.Location = model.location;

                await DbEntities.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditImage(int post_id,  string imagePath)
        {
            try
            {
                Image image = await FindPost(post_id);
                image.ImagePath = imagePath;
                await DbEntities.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> ClearImages()
        {
            try
            {
                DbEntities.Images.RemoveRange(DbEntities.Images);
                await DbEntities.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> ClearAccounts()
        {
            try
            {
                DbEntities.AspNetUsers.RemoveRange(DbEntities.AspNetUsers);
                await DbEntities.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}