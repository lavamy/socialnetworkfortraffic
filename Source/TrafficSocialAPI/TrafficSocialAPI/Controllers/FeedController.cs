﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TrafficSocialAPI.Database;
using TrafficSocialAPI.Manager;
using TrafficSocialAPI.Models;

namespace TrafficSocialAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [Authorize]
    [RoutePrefix("api/newsfeed")]
    public class FeedController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        private FeedManager feedManager = FeedManager.getInstance();

        [HttpPost]
        [Route("post")]
        public async Task<IHttpActionResult> PostImage(ImageBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return BadRequest("User not found");
            }

            string result = await feedManager.PostImage(user, model);

            if (result == null)
                return BadRequest("Can't save image");

            return Ok(result);
        }

        [HttpGet]
        [Route("browser")]
        public IHttpActionResult GetNewsFeed([FromUri]int count, [FromUri]string from_date = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            //if (user == null)
            //{
            //    return BadRequest("User not found");
            //}
            DateTime fromDay = DateTime.Now;
            if (!String.IsNullOrEmpty(from_date))
                if (!DateTime.TryParse(from_date, out fromDay))
                    return BadRequest("Error parsing date");

            IEnumerable<NewsFeedResponseModel> response = feedManager.GetNewsFeed(fromDay, count);
            if (response == null)
                return BadRequest("Error getting post list");

            return Ok(response);
        }

        [HttpGet]
        [Route("newest")]
        public async Task<IHttpActionResult> GetNewsFeed([FromUri]string from_date)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DateTime fromDay = DateTime.Now;
            if (!DateTime.TryParse(from_date, out fromDay))
                return BadRequest("Error parsing date");

            IEnumerable<NewsFeedResponseModel> response = await feedManager.GetNewsFeed(fromDay);
            if (response == null)
                return BadRequest("Error getting post list");

            return Ok(response);
        }

        [HttpPut]
        [Route("deletefeed")]
        public async Task<IHttpActionResult> DeleteFeed([FromUri]int post_id)
        {
            if (!(await feedManager.DeleteFeed(post_id)))
                return BadRequest();
            return Ok();
        }

        [HttpPost]
        [Route("updatefeed")]
        public async Task<IHttpActionResult> UpdateFeed(NewsFeedUpdateBindingModel model)
        {
            if (!(await feedManager.UpdateFeed(model)))
                return BadRequest();
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("clear")]
        public async Task<IHttpActionResult> ClearDatabase()
        {
            if (!(await feedManager.ClearImages()))
                return BadRequest("Error clearing database");
            return Ok();
        }
    }
}
