﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using TrafficSocialAPI.Controllers;
using TrafficSocialAPI.Database;
using TrafficSocialAPI.Models;

namespace TrafficSocialAPI.Manager
{
    public class FeedManager
    {
        private static FeedManager currentObject;

        public static FeedManager getInstance()
        {
            if (currentObject == null)
                currentObject = new FeedManager();
            return currentObject;
        }

        private const string ImagePathFolder = @"/Gallery/";
        private static string HostFullPath = HostingEnvironment.MapPath(ImagePathFolder);
        private const string ImageExtension = ".jpg";
        private DatabaseManager dbManager = DatabaseManager.getInstance();

        private FeedManager()
        {
            
        }

        public async Task<string> PostImage(ApplicationUser user, ImageBindingModel model)
        {
            string image_path = CreateImageFile(model.image);

            //Update the database
            Image imageDb = new Image()
            {
                ImagePath = image_path,
                Location = model.location,
                Description = model.description,
                ErrorWarning = model.error_warning,
                LocationDetail = model.location_detail,
                Warning = model.warning,
                Note = model.note,
                Province = model.province,
                Route = model.route
            };

            if (await dbManager.PostImage(user, imageDb))
                return image_path;
            return null;
        }

        private string CreateImageFile(string encodedImage)
        {
            string image_name = RandomSequenceHelper.Generate(128) + ImageExtension;
            string image_path = ImagePathFolder + image_name;
            string host_fullpath = HostFullPath + image_name;

            //Create image file
            var bytes = Convert.FromBase64String(encodedImage);
            using (var imageFile = new FileStream(host_fullpath, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
            return image_path;
        }

        private void DeleteImageFile(string image_path)
        {
            int index = image_path.IndexOf(ImagePathFolder);
            string image_name = image_path.Substring(index + ImagePathFolder.Length);
            string host_fullpath = HostFullPath + image_name;

            File.Delete(host_fullpath);
        }

        public IEnumerable<NewsFeedResponseModel> GetNewsFeed(DateTime from_day, int count)
        {
            List<Image> resultDb = dbManager.GetPosts(from_day, count);
            if (resultDb == null)
                return null;

            return resultDb.Select(i => new NewsFeedResponseModel(i));
        }

        public async Task<IEnumerable<NewsFeedResponseModel>> GetNewsFeed(DateTime from_day)
        {
            List<Image> resultDb = await dbManager.GetPosts(from_day);
            if (resultDb == null)
                return null;

            return resultDb.Select(i => new NewsFeedResponseModel(i));
        }

        public async Task<bool> UpdateFeed(NewsFeedUpdateBindingModel model)
        {
            if (model.image != "null")
            {
                string old_imagePath = (await dbManager.FindPost(model.post_id)).ImagePath;
                string image_path = CreateImageFile(model.image);
                if (await dbManager.EditImage(model.post_id, image_path))
                {
                    DeleteImageFile(old_imagePath);
                }
            }

            return await dbManager.EditPost(model);
        }

        public async Task<bool> DeleteFeed(int postID)
        {
            return await dbManager.DeletePost(postID);
        }

        public async Task<bool> ClearImages()
        {
            if (!await dbManager.ClearImages())
                return false;

            DirectoryInfo downloadedMessageInfo = new DirectoryInfo(HostFullPath);

            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
            {
                file.Delete();
            }
            return true;
        }
    }
}