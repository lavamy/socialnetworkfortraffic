﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TrafficSocialAPI.Database;

namespace TrafficSocialAPI.Models
{
    public class NewsFeedResponseModel
    {
        public int post_id { get; set; }
        public DateTime post_date { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string image_link { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public string error_warning { get; set; }
        public string province { get; set; }
        public string route { get; set; }
        public string location_detail { get; set; }
        public string warning { get; set; }
        public string note { get; set; }

        public NewsFeedResponseModel()
        {
            
        }

        public NewsFeedResponseModel(Image image)
        {
            post_id = image.Id;
            description = image.Description;
            location = image.Location;
            post_date = image.PostDate;
            image_link = image.ImagePath;
            error_warning = image.ErrorWarning;
            location_detail = image.LocationDetail;
            note = image.Note;
            province = image.Province;
            route = image.Route;
            warning = image.Warning;
            username = image.AspNetUser.UserName;
            name = image.AspNetUser.Name;
        }


    }
}