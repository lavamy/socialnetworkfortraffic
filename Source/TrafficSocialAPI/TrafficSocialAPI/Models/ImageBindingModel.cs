﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrafficSocialAPI.Models
{
    public class ImageBindingModel
    {
        [Required]
        public string image { get; set; }

        public string userID { get; set; }

        public string description { get; set; }
        public string location { get; set; }
        public string error_warning { get; set; }
        public string province { get; set; }
        public string route { get; set; }
        public string location_detail { get; set; }
        public string warning { get; set; }
        public string note { get; set; }
    }
}