﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrafficSocialAPI.Models
{
    public class UserInfoResponseModel
    {
        public string email { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
        public bool is_external_login { get; set; }
    }
}