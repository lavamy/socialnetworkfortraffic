﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrafficSocialAPI.Models
{
    public class UpdateInfoBindingModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string name { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string phone_number { get; set; }
    }
}