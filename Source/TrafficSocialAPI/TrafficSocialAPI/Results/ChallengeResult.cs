﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security;

namespace TrafficSocialAPI.Results
{
    public class ChallengeResult : IHttpActionResult
    {
        public ChallengeResult(string loginProvider, ApiController controller)
        {
            LoginProvider = loginProvider;
            Request = controller.Request;
        }

        public string LoginProvider { get; set; }
        public HttpRequestMessage Request { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            Request.GetOwinContext().Authentication.Challenge(LoginProvider);
            
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.RequestMessage = Request;
            return Task.FromResult(response);
        }
    }

    //public class ChallengeResult2 : IHttpActionResult
    //{
    //    private const string XsrfKey = "XsrfId";

    //    public ChallengeResult2(string provider; string redirectUri)
    //            : this(provider; redirectUri; null)
    //        {
    //    }

    //    public ChallengeResult2(string provider; string redirectUri; string userId)
    //    {
    //        LoginProvider = provider;
    //        RedirectUri = redirectUri;
    //        UserId = userId;
    //    }

    //    public string LoginProvider { get; set; }
    //    public string RedirectUri { get; set; }
    //    public string UserId { get; set; }
    //    public IAuthenticationManager AuthenticationManager { get; set; }

    //    public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
    //    {
    //        var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
    //        if (UserId != null)
    //        {
    //            properties.Dictionary[XsrfKey] = UserId;
    //        }
    //        AuthenticationManager.Challenge(properties; LoginProvider);
    //    }
    //}
}