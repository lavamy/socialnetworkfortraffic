﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http.Cors;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using TrafficSocialAPI.Models;
using TrafficSocialAPI.Providers;
using TrafficSocialAPI;

namespace TrafficSocialAPI
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication; please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/authentication/signin"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/authentication/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "";
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "";
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "159448824417867",
            //    appSecret: "be775f322d9732d6abbb1eb9659a22b3");


            var google = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "757191784687-e85ajed4t9s8nu567k5tgmc82rvedeo3.apps.googleusercontent.com",
                ClientSecret = "HA0UoaW4IlTucyhq0vARsCyB",
                Provider = new GoogleOAuth2AuthenticationProvider()
                {
                    OnAuthenticated = async context =>
                    {
                        // Retrieve the OAuth access token to store for subsequent API calls
                        //string accessToken = context.AccessToken;
                        
                        // Retrieve the name of the user in Google
                        External_Name = context.Name;
                        // Retrieve the user's email address
                        External_Email = context.Email;
                        External_Identity = context.Identity;
                        // You can even retrieve the full JSON-serialized user
                        //var serializedUser = context.User;
                    }
                }
            };
            google.Scope.Add("https://www.googleapis.com/auth/userinfo.profile");
            google.Scope.Add("https://www.googleapis.com/auth/userinfo.email");
            app.UseGoogleAuthentication(google);
        }

        public static string External_Email = "";
        public static string External_Name = "";
        public static ClaimsIdentity External_Identity = null;
    }
}