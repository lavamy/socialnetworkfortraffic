﻿CREATE TABLE [dbo].[Image]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [UserID] NVARCHAR(128) NULL, 
    [Status] NVARCHAR(MAX) NULL , 
    [ImageName] NVARCHAR(256) NOT NULL, 
    [Location] NCHAR(100) NULL, 
    CONSTRAINT [FK_ImageTable_AspNetUsers] FOREIGN KEY (UserID) REFERENCES AspNetUsers(Id)
)
