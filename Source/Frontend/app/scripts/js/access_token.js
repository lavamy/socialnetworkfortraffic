var $url = window.location.href;
var $prefix = $redirectUri + "#access_token=";
if ($url.startsWith($prefix)) {
    var $param = $url.substring($prefix.length, $url.length);
    var $token = $param.split("&")[0];
    console.log("Token = " + $token);
    console.log("Service = " +     angular.injector(['ng', 'yapp']).get("sharedService"));
    console.log("Service Name = " +     angular.injector(['ng', 'yapp']).get("sharedService"));
    angular.injector(['ng', 'yapp']).get("sharedService").setToken($token);
    window.location.href = $redirectUri + "#/dashboard/home";
}
