'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
    .module('yapp', [
        'ui.router',
        'snap',
        'ngAnimate',
        'LocalStorageModule'
    ])
    .config(function ($stateProvider, $urlRouterProvider, localStorageServiceProvider) {

        localStorageServiceProvider.setPrefix('tlt');
        localStorageServiceProvider.setStorageType('localStorage');

        $urlRouterProvider.when('/dashboard', '/dashboard/home');
        $urlRouterProvider.otherwise('/dashboard');

        $stateProvider
            .state('base', {
                abstract: true,
                url: '',
                templateUrl: 'views/base.html'
            })
            .state('logout', {
                url: '/logout',
                parent: 'base',
                templateUrl: 'views/base.html',
                controller: 'LogoutCtrl'
            })
            .state('login', {
                url: '/login',
                parent: 'base',
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .state('dashboard', {
                url: '/dashboard',
                parent: 'base',
                templateUrl: 'views/dashboard.html',
                controller: 'DashboardCtrl'
            })
            .state('home', {
                url: '/home',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/home.html',
                controller: 'HomeCtrl'
            })
            .state('post', {
                url: '/post',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/post.html',
                controller: 'PostCtrl'
            })
            .state('map', {
                url: '/map',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/map.html',
                controller: 'MapCtrl'
            })
            .state('account', {
                url: '/account',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/account.html',
                controller: 'AccountCtrl'
            })
            .state('about', {
                url: '/about',
                parent: 'dashboard',
                templateUrl: 'views/dashboard/about.html'
            })

    })
    .service('mapservice', function () {
        var defaultCenter = new google.maps.LatLng(15.5174539, 104.1663867);
        var general_center = 6;
        var specific_center = 12;

        var icon = 'images/icon.png';
        var markers;
        var map;
        var infowindow;
        var visibleFeeds = [];

        var centerCoord;
        var clickCoord;
        var clickMarker;
        var isClickMarker;
        var mode = 'general';
        var isUpdated = false;

        var geocoder = new google.maps.Geocoder;
        this.init = function (current, initmarkerlist, mode) {

            markers = [];

            infowindow = new google.maps.InfoWindow({ maxWidth: 300 });

            var initZoom;
            if (current == null) {
                centerCoord = defaultCenter;
                initZoom = general_center;
            } else {
                var location_split = current.split(',');
                centerCoord = new google.maps.LatLng(location_split[0], location_split[1]);
                initZoom = specific_center;
            }

            var options = {
                center: centerCoord,
                zoom: initZoom
                //disableDefaultUI: true   
            }
            map = new google.maps.Map(
                document.getElementById("map"), options
                );

            this.initMode(initmarkerlist, mode);
        };

        this.initMode = function (initmarkerlist, new_mode) {
            if (new_mode === null)
                mode = 'general';
            else
                mode = new_mode;

            if (mode === 'general' || mode === 'specific') {
                isClickMarker = false;
                this.setFeeds(initmarkerlist);
            } else if (mode === 'post' || mode === 'edit') {
                isClickMarker = true;
                this.clearMarkers();
                visibleFeeds = [];
                // alert(mode);
                clickMarker = new google.maps.Marker({
                    map: map,
                    title: 'Chọn vị trí này',
                    animation: google.maps.Animation.DROP
                });

                clickCoord = null;

                if (mode === 'edit') {
                    var location_split = initmarkerlist[0].location.split(',');
                    // alert('edit ' + initmarkerlist[0]);                    
                    clickMarker.setPosition(new google.maps.LatLng(location_split[0], location_split[1]));
                }
            }

            google.maps.event.addListener(map, 'click', function (e) {
                if (isClickMarker === true) {
                    if (mode === 'edit') {
                        if (!confirm('Bạn có chắc muốn thay đổi địa điểm ?'))
                        {
                            isUpdated = false;
                            return;
                        } else 
                            isUpdated = true;
                    }

                    clickCoord = e.latLng;

                    clickMarker.setPosition(clickCoord);
                    geocoder.geocode({ 'location': clickCoord }, function (results, status) {
                        var content;
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                content = '<div style="color:black;">' + 'Địa chỉ: ' + results[1].formatted_address + '</div>';
                            } else {
                                content = '<div style="color:black;">' + 'Không tìm thấy địa chỉ' + '</div>';
                            }
                        } else {
                            content = '<div style="color:black;">' + 'Không tìm thấy địa chỉ' + '</div>';
                        }
                        infowindow.setContent(content);
                        infowindow.open(map, clickMarker);
                    });
                } else {
                    infowindow.close();
                }
            });
        }
        
        this.getIsUpdated = function() {
            return isUpdated;
        }

        this.getClickCoordinate = function () {
            return clickCoord;
        }

        this.setFeeds = function (newsfeed) {
            if (isClickMarker)
                return;
            if (visibleFeeds.length !== newsfeed.length) {
                visibleFeeds = newsfeed;
                this.drop();
            }
        }

        // this.refresh = function (newsfeed) {
        //     if (visibleFeeds.length !== newsfeed.length) {
        //         visibleFeeds = newsfeed;
        //         //this.init(new google.maps.LatLng(10.8231812,106.594644), newsfeed);
        //         this.drop();
        //     }
        // }

        this.drop = function () {
            this.clearMarkers();
            for (var i = 0; i < visibleFeeds.length; i++) {
                this.addMarkerWithTimeout(visibleFeeds[i], (i + 1) * 500);
            }
        }

        this.addMarkerWithTimeout = function (feed, timeout) {
            window.setTimeout(function () {
                var location_split = feed.location.split(',');
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(location_split[0], location_split[1]),
                    map: map,
                    icon: icon,
                    title: feed.description,
                    animation: google.maps.Animation.DROP
                });
                marker.addListener('click', function () {
                    
                    //format datetime
                    var unix_timestamp = Date.parse(feed.post_date);
                    var date = new Date(unix_timestamp);
                    // Hours part from the timestamp
                    var hours = date.getHours();
                    // Minutes part from the timestamp
                    var minutes = "0" + date.getMinutes();
                    // Seconds part from the timestamp
                    var seconds = "0" + date.getSeconds();
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    if (month < 10)
                        month = "0" + month;
                    var year = date.getFullYear();
                    // Will display time in 10:30:23 format
                    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + " " + day + "/" + month + "/" + year;

                    var content = '<div id="iw-container">' +
                        '<div class="iw-title">' + feed.name + ' đăng tin</div>' +

                        '<div class="iw-content">' +
                        '<div class="iw-subTitle">' + 'Thời gian:' + '</div>' +
                        '<p>' + formattedTime + '</p>' +
                        '<div class="iw-subTitle">' + 'Chi tiết' + '</div>' +
                        '<img src="' + $host + feed.image_link + '" alt="Hình ảnh tình trạng" height="120" width="100">' +
                        '<p>Lỗi cảnh báo: ' + feed.error_warning + '</p>' +
                        '<p>Mô tả: ' + feed.description + '</p>' +
                        '<p>Cảnh báo: ' + feed.warning + '</p>' +
                        '<p>Ghi chú: ' + feed.note + '</p>' +
                        '<div class="iw-subTitle">Địa điểm</div>' +
                        '<p>Tỉnh thành: ' + feed.province + '</p>' +
                        '<p>Tuyến đường: ' + feed.route + '</p>' +
                        '<p>Chi tiết vi trí: ' + feed.location_detail +
                        '</div>' +
                        '<div class="iw-bottom-gradient"></div>' +
                        '</div>';

                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                });

                marker.addListener('mouseover', function () {
                    // if (marker.getAnimation() !== null) {
                    //     marker.setAnimation(null)
                    // } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                    //}
                });

                marker.addListener('mouseout', function () {
                    marker.setAnimation(null);
                });
                //console.log('add marker');
                markers.push(marker, timeout);
            });
        };

        this.clearMarkers = function () {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        };


    })
    .service('sharedService', function ($rootScope, $http, localStorageService) {
        var KEY_TOKEN = "token";
        var allFeeds = new Array();
        var hiddenFeeds = new Array();
        var busy = false;
        var accountUpdateSuccess = false;
        var canLoadMore = true;
        var user;

        this.getInfo = function () {
            return user;
        }

        this.setAccountUpdateSuccess = function (value) {
            accountUpdateSuccess = value;
        }

        this.setAccountUpdateSuccess = function (value) {
            accountUpdateSuccess = value;
        }

        this.setToken = function (token) {
            console.log("setToken(" + token + ")");
            if (localStorageService.isSupported) {
                if (token) {
                    localStorageService.set(KEY_TOKEN, token);
                }
                else
                    localStorageService.remove(KEY_TOKEN);

            } else {
                console.log("unsupported");
            }
        }

        this.getToken = function () {
            if (localStorageService.isSupported) {
                var token = localStorageService.get(KEY_TOKEN);
                //console.log("getToken() = " + token);
                return token;
            } else {
                console.log("unsupported");
            }
        }

        this.loadNewerFeeds = function () {
            console.log("Loading newer feeds")
            console.log("TOKEN:" + this.getToken());
            var mileStone = null;
            if (allFeeds.length > 0) {
                console.log(allFeeds[0]);
                mileStone = allFeeds[0].post_date + "Z";
                console.log("Existed " + mileStone);
            } else {
                console.log("Current datetime: " + mileStone);
            }
            var req = {
                method: 'GET',
                url: $host + "/api/newsfeed/newest",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + this.getToken()
                },
                params: {
                    from_date: mileStone
                }
            }
            $http(req).then(function (response) {
                console.log("newer feeds");
                console.log(response);
                if (response && response.data && response.data.length) {
                    console.log("current");
                    console.log(allFeeds);
                    for (var i = 0; i < response.data.length; i++) {
                        var valid = true;
                        for (var j = 0; j < allFeeds.length; j++) {
                            if (allFeeds[j].post_id === response.data[i].post_id) {
                                valid = false;
                                break;
                            }
                        }
                        if (valid) { 
                        allFeeds.splice(0, 0, response.data[i]);
                    }
                }
                console.log(allFeeds);
            }
            }, function (response) {
            console.log("Load failed");
            console.log(response);
        });
        }

function load(token) {
    var mileStone = new Date().toISOString();
    if (allFeeds.length > 0) {
        mileStone = allFeeds[allFeeds.length - 1].post_date + "Z";
        console.log("Existed " + mileStone);
    } else {
        console.log("Current datetime: " + mileStone);
    }
    //alert(mileStone);
    var req = {
        method: 'GET',
        url: $host + "/api/newsfeed/browser",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
        },
        params: {
            count: 5,
            from_date: mileStone
        }
    }
    $http(req).then(function (response) {
        console.log(response);
        if (response && response.data && response.data.length) {
            Array.prototype.push.apply(allFeeds, response.data.reverse())
        } else {
            canLoadMore = false;
        }
        busy = false;                
        // service.loadOlderFeeds();                
    }, function (response) {
        console.log(response);
        busy = false;
    });

}

this.getUser = function () {
    return user;
}

this.loadOlderFeeds = function () {
    if (busy == true) {
        return;
    }
    busy = true;
    var token = this.getToken();
    if (user === undefined) {
        var req = {
            method: 'GET',
            url: $host + "/api/authentication/info",
            headers: {
                'Authorization': 'Bearer ' + this.getToken()
            },
        }
        $http(req).then(function (response) {
            user = response.data;
            console.log(user);
            load(token);
        }, function (response) {
            console.log(response);
            busy = false;
        });
    } else {
        load(token);
    }
}

this.haveOlderFeeds = function () {
    return canLoadMore;
}
this.isBusy = function () {
    return busy;
}

this.getfeed = function (post_id) {
    for (var i = 0; i < allFeeds.length - 1; i++) {
        if (allFeeds[i].post_id == post_id)
            return allFeeds[i];
    }
    console.log("feed null");
    return null;
}

this.getAllFeeds = function () {
    if (allFeeds.length == 0) {
        this.loadOlderFeeds();
        console.log("feed null");
    }
    return allFeeds;
}

this.hideFeed = function (post_id) {
    console.log("hide");
    if (hiddenFeeds.indexOf(post_id) >= 0) {
        return false;
    }
    hiddenFeeds.push(post_id);
    for (var i = 0; i < allFeeds.length; i++) {
        if (allFeeds[i].post_id === post_id) {
            allFeeds.splice(i, 1);
            return true;
        }
    }
}

this.getHiddenFeeds = function () {
    return hiddenFeeds;
}
    })
    .directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);

            element.on('change', function (onChangeEvent) {
                var reader = new FileReader();

                reader.onload = function (onLoadEvent) {
                    scope.$apply(function () {
                        fn(scope, { $fileContent: onLoadEvent.target.result });
                    });
                };

                reader.readAsDataURL((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
            });
        }
    };
});;

var $host = "http://trafficsocial.azurewebsites.net";
var $redirectUri = "http://trafficsocial.azurewebsites.net";

