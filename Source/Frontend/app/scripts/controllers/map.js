'use strict';

angular.module('yapp')
.controller('MapCtrl', function($scope, $interval, $location, mapservice, sharedService) {
    
    $scope.feeds = [];
    $scope.center = $location.search().location;
    $scope.mode = $location.search().mode;
    
    $scope.init = function(center) {
        mapservice.init(center, $scope.feeds,$scope.mode );
    }
    
    $scope.refresh = function() {
        $scope.feeds = sharedService.getAllFeeds();        
        mapservice.setFeeds($scope.feeds);
    };

    $interval( function()
    { 
        $scope.refresh(); 
    }
    ,2000);

    $scope.init($scope.center);
});