'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp').controller('LoginCtrl', function ($scope, $location, $http, sharedService) {
    $scope.validEmail = true;
    $scope.validPassword = true;
    $scope.validPassword2 = true;
    $scope.validName = true;

    $scope.$on('$viewContentLoaded', function () {
        if (sharedService.getToken() != null) {
            $location.path('/dashboard');
        }
    });

    $scope.switchSignInOption = function (isSignUp) {
        if ($scope.isSending) return;
        $scope.isSignUp = isSignUp;
        $scope.message = ""
        $scope.validEmail = true;
        $scope.validPassword = true;
        $scope.validPassword2 = true;
        $scope.validName = true;
    }

    $scope.submit = function () {
        if ($scope.isSending) return;

        if ($scope.isSignUp) {
            $scope.validEmail = ($scope.email && $scope.email.length >= 5 && $scope.email.indexOf("@") > -1 && $scope.email.indexOf(".") > -1);
            $scope.validPassword = ($scope.password && $scope.password.length >= 6 &&
                /[A-Z]/.test($scope.password) && /[0-9]/.test($scope.password) && /[!@#\$%\^&\*]/.test($scope.password));
            $scope.validPassword2 = ($scope.password2 && $scope.password2 === $scope.password);
            $scope.validName = ($scope.name && $scope.name.length >= 6);
        }

        if ($scope.validEmail && $scope.validPassword) {
            if ($scope.isSignUp) {
                if ($scope.validPassword2 && $scope.validName) {
                    return signUp($scope.email, $scope.password, $scope.name);
                }
            } else {
                return signIn($scope.email, $scope.password);
            }
        }
    }

    $scope.loadExternalLogins = function () {
        var req = {
            method: 'GET',
            url: $host + "/api/authentication/ExternalLogins?returnUrl=" + encodeURIComponent($redirectUri) + "&generateState=true",
        }
        $http(req).then(function (response) {
            $scope.externalLogins = response.data;
            for (var i = 0; i < response.data.length; i++) {
                var provider = $scope.externalLogins[i];
                if (provider.Name === 'Google') {
                    $scope.GoogleLoginAvailable = true;
                } else if (provider.Name === 'Facebook') {
                    $scope.FacebookLoginAvailable = true;
                }
            }
        });
    }

    $scope.login = function ($provider) {
        if ($scope.isSending) return;
        for (var i = 0; i < $scope.externalLogins.length; i++) {
            var provider = $scope.externalLogins[i];
            if (provider.Name === $provider) {
                window.location.href = $host + provider.Url;
                return true;
            }
        }

        return true;
    }

    function signIn(email, password) {
        $scope.isSending = true;
        var dataStr = "grant_type=password&username=" + email + "&password=" + password;
        var req = {
            method: 'POST',
            url: $host + "/api/authentication/signin",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: dataStr
        }
        $http(req).then(function (response) {
            $scope.isSending = false;
            sharedService.setToken(response.data.access_token);
            $location.path('/dashboard');
        }, function (response) {
            $scope.isSending = false;
            $scope.message = "Email hoặc mật khẩu không đúng";
        });
        return true;
    }

    function signUp(email, password, name) {
        $scope.isSending = true;
        var req = {
            method: 'POST',
            url: $host + "/api/authentication/signup",
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                Name: name,
                Email: email,
                Password: password
            }
        }
        $http(req).then(function (response) {
            $scope.isSending = false;
            $scope.message = "Đăng ký thành công!";
            signIn(email, password);
        }, function (response) {
            $scope.isSending = false;
            $scope.message = response.data.ModelState[''][0];
        });
        return true;
    }
    
    
});
