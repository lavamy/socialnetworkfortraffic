'use strict';

angular.module('yapp')
    .controller('PostCtrl', function ($scope, $http, $location, $interval, mapservice, sharedService) {

        if ($location.search().post_id != null) {
            $scope.isSending = true;
            $scope.isUpdating = false;
            $scope.title = "Cập Nhật Tình Hình  Giao Thông";
        }
        else {
            $scope.title = "Đăng Tin Tình Hình  Giao Thông";
            $scope.isSending = false;
            $scope.isUpdating = true;
        }

        $scope.cities = [
            "An Giang",
            "Bà Rịa - Vũng Tàu",
            "Bắc Giang",
            "Bắc Kạn",
            "Bạc Liêu",
            "Bắc Ninh",
            "Bến Tre",
            "Bình Định",
            "Bình Dương",
            "Bình Phước",
            "Bình Thuận",
            "Cà Mau",
            "Cao Bằng",
            "Đắk Lắk",
            "Đắk Nông",
            "Điện Biên",
            "Đồng Nai",
            "Đồng Tháp",
            "Gia Lai",
            "Hà Giang",
            "Hà Nam",
            "Hà Tĩnh",
            "Hải Dương",
            "Hậu Giang",
            "Hòa Bình",
            "Hưng Yên",
            "Khánh Hòa",
            "Kiên Giang",
            "Kon Tum",
            "Lai Châu",
            "Lâm Đồng",
            "Lạng Sơn",
            "Lào Cai",
            "Long An",
            "Nam Định",
            "Nghệ An",
            "Ninh Bình",
            "Ninh Thuận",
            "Phú Thọ",
            "Quảng Bình",
            "Quảng Nam",
            "Quảng Ngãi",
            "Quảng Ninh",
            "Quảng Trị",
            "Sóc Trăng",
            "Sơn La",
            "Tây Ninh",
            "Thái Bình",
            "Thái Nguyên",
            "Thanh Hóa",
            "Thừa Thiên Huế",
            "Tiền Giang",
            "Trà Vinh",
            "Tuyên Quang",
            "Vĩnh Long",
            "Vĩnh Phúc",
            "Yên Bái",
            "Phú Yên",
            "Cần Thơ",
            "Đà Nẵng",
            "Hải Phòng",
            "Hà Nội",
            "TP HCM"
        ];

        $scope.$on('$viewContentLoaded', function () {
            var post_id = $location.search().post_id;
            if (post_id != null) {
                var feed = sharedService.getfeed(post_id);

                $scope.errorWarning = feed.error_warning;
                $scope.province = $scope.cities[1];
                $scope.route = feed.route;
                $scope.locationDetail = feed.location_detail;
                $scope.warning = feed.warning;
                $scope.description = feed.description;
                $scope.note = feed.note;
                mapservice.init(feed.location, [feed], 'edit');
            }
            else {
                mapservice.init(null, null, 'post');
            }
        });

        $scope.feeds = [];

        $scope.refresh = function () {
            $scope.feeds = sharedService.getAllFeeds();
            mapservice.setFeeds($scope.feeds);
        };

        $interval(function () {
            $scope.refresh();
        }, 3000);

        $scope.validErrorWarning = true;
        $scope.validProvince = true;
        $scope.validRoute = true;
        $scope.validLocationDetail = true;
        $scope.validWarning = true;
        $scope.validDescription = true;
        $scope.validNote = true;
        $scope.validImage = true;
        $scope.validLocation = true;

        $scope.showContent = function ($fileContent) {
            $scope.image = $fileContent;
        };

        var loTemp;
        var lo;

        $scope.submit = function () {
            $scope.validErrorWarning = true;
            $scope.validProvince = true;
            $scope.validRoute = true;
            $scope.validLocationDetail = true;
            $scope.validWarning = true;
            $scope.validDescription = true;
            $scope.validNote = true;
            $scope.validLocation = true;
            $scope.validImage = true;

            var valid = true;

            lo = mapservice.getClickCoordinate();

            if (undefined === $scope.errorWarning || $scope.errorWarning.length < 1) {
                valid = false;
                $scope.validErrorWarning = false;
            }
            if (undefined === $scope.province || $scope.province.length < 1) {
                valid = false;
                $scope.validProvince = false;
            }
            if (undefined === $scope.route || $scope.route.length < 1) {
                valid = false;
                $scope.validRoute = false;
            }
            if (undefined === $scope.locationDetail || $scope.locationDetail.length < 1) {
                valid = false;
                $scope.validLocationDetail = false;
            }
            if (undefined === $scope.warning || $scope.warning.length < 1) {
                valid = false;
                $scope.validWarning = false;
            }
            if (undefined === $scope.description || $scope.description.length < 1) {
                valid = false;
                $scope.validDescription = false;
            }
            if (undefined === $scope.note || $scope.note.length < 1) {
                valid = false;
                $scope.validNote = false;
            }
            if ($location.search().post_id == null && undefined === $scope.image) {
                valid = false;
                $scope.validImage = false;
            }
            if (undefined === $scope.note || $scope.note.length < 1) {
                valid = false;
                $scope.validNote = false;
            }
            if ($location.search().post_id == null && lo == null) {
                valid = false;
                $scope.validLocation = false;
            }

            if (valid) {
                if ($location.search().post_id != null)
                    $scope.isUpdating = true;
                else
                    $scope.isSending = true;

                $scope.isLoading = true;

                var req;
                if ($location.search().post_id != null) {

                    if ($location.search().post_id != null && mapservice.getIsUpdated() == false)
                        loTemp = "null"
                    else
                        loTemp = lo.lat() + "," + lo.lng();

                    if (undefined === $scope.image)
                        $scope.image = "null";
                    else {
                        $scope.image = $scope.image.replace("data:image/jpeg;base64,", "");
                        $scope.image = $scope.image.replace("data:image/png;base64,", "");
                    }

                    req = {
                        method: 'POST',
                        url: $host + "/api/newsfeed/updatefeed",
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + sharedService.getToken()
                        },
                        data: {
                            post_id: $location.search().post_id,
                            image: $scope.image,
                            description: $scope.description,
                            location: loTemp,
                            error_warning: $scope.errorWarning,
                            province: $scope.province,
                            route: $scope.route,
                            location_detail: $scope.locationDetail,
                            warning: $scope.warning,
                            note: $scope.note
                        }
                    };
                }
                else {
                    $scope.image = $scope.image.replace("data:image/jpeg;base64,", "");
                    $scope.image = $scope.image.replace("data:image/png;base64,", "");
                    req = {
                        method: 'POST',
                        url: $host + "/api/newsfeed/post",
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + sharedService.getToken()
                        },
                        data: {
                            image: $scope.image,
                            description: $scope.description,
                            location: lo.lat() + "," + lo.lng(),
                            error_warning: $scope.errorWarning,
                            province: $scope.province,
                            route: $scope.route,
                            location_detail: $scope.locationDetail,
                            warning: $scope.warning,
                            note: $scope.note
                        }
                    };
                }

                $http(req).then(function (response) {
                    console.log(response);
                    if ($location.search().post_id != null) {
                        $scope.message = "Cập nhật thành công!";
                        $scope.isUpdating = false;
                    }
                    else {
                        $scope.message = "Bài đã được đăng thành công!";
                        $scope.isSending = false;
                    }
                    $scope.isLoading = false;

                }, function (response) {
                    console.log(response);
                    if ($location.search().post_id != null) {
                        $scope.message = "Không thể cập nhật";
                        $scope.isUpdating = false;
                    }
                    else {
                        $scope.message = "Không thể đăng bài";
                        $scope.isSending = false;
                    }
                    $scope.isLoading = false;
                });

                return true;
            }
            return false;
        };
    });
