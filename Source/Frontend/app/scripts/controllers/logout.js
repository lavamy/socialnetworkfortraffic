'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp').controller('LogoutCtrl', function ($scope, $location, $http, sharedService) { 
      $scope.logOut = function() {
        sharedService.setToken(null);
        $location.path('login');
    }
    
    $scope.$on('$viewContentLoaded', function(){
         $scope.logOut();
    });
});
