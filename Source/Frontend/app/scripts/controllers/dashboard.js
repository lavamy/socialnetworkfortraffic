'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
    .controller('DashboardCtrl', function ($scope, $location, $http, $state, sharedService) {
        $scope.$state = $state;

        $scope.getAccountName = function () {
            var model = sharedService.getUser();
            if (model === undefined) {
                return "";
            }
            return model.name;
        }


        $scope.$on('$viewContentLoaded', function () {
            if (sharedService.getToken() == null) {
                $location.path('/login');
            }
        });
    });
