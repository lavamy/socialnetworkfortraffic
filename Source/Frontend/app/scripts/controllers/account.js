'use strict';

angular.module('yapp').controller('AccountCtrl', function ($scope, $location, $http, sharedService) {
    //Test account type (local or google or facebook)
    $scope.accountType = true;

    //get info account
    var req = {
        method: 'GET',
        url: $host + "/api/authentication/info",
        headers: {
            'Authorization': 'Bearer ' + sharedService.getToken()
        },
    }

    $http(req).then(function (response) {
        $scope.user = response.data;
        //if account is external (google), it hide change password.
        if (response.data.is_external_login) {
            $scope.accountType = false;
        }

    }, function (response) {
    });

    //update acount
    $scope.name = true;
    $scope.phonenumber = true;
    $scope.updateAccount = function () {
        $scope.name = ($scope.user.name && $scope.user.name.length >= 6);

        if (!$scope.name)
            $scope.message = "Họ tên có ít nhất 6 ký tự";

        var regexp = /^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/;
        $scope.phonenumber = regexp.test($scope.user.phone_number);

        if ($scope.name && !$scope.phonenumber)
            $scope.message = "Số điện thoại không hợp lệ";

        if ($scope.name && $scope.phonenumber) {

            var req = {
                method: 'POST',
                url: $host + "/api/authentication/updateinfo",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + sharedService.getToken()
                },
                data: {
                    name: $scope.user.name,
                    phone_number: $scope.user.phone_number
                }
            }
            $http(req).then(function (response) {
                $scope.message = "Cập nhật tài khoản thành công";
                var model = sharedService.getUser();
                model.name = $scope.user.name;
            }, function (response) {
                $scope.message = "error";
            });
        }
    }
    
    //change password
    $scope.changePassword = function () {
        var req = {
            method: 'POST',
            url: $host + "/api/authentication/changepassword",
            headers: {
                'Authorization': 'Bearer ' + sharedService.getToken()
            },
            data: {
                OldPassword: $scope.oldPassword,
                NewPassword: $scope.newPassword,
                ConfirmPassword: $scope.newPassword1
            }
        }
        $http(req).then(function (response) {
            $scope.message1 = "Cập nhật mật khẩu thành công";
        }, function (response) {
            if (response.data.ModelState[""] != null)
                $scope.message1 = response.data.ModelState[""][0];
            if (response.data.ModelState["model.NewPassword"] != null)
                $scope.message1 = response.data.ModelState["model.NewPassword"][0];
            if (response.data.ModelState["model.OldPassword"] != null)
                $scope.message1 = response.data.ModelState["model.OldPassword"][0];
            if (response.data.ModelState["model.ConfirmPassword"] != null)
                $scope.message1 = response.data.ModelState["model.ConfirmPassword"][0];
        });
    }

});
