'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp').controller('HomeCtrl', function ($scope, $location, $http, sharedService) {
    $scope.init = function () {
        $scope.service = sharedService;
    }
    $scope.formatDateTime = function (dateTime) {
        //format datetime
        var unix_timestamp = Date.parse(dateTime);
        var date = new Date(unix_timestamp);
        // Hours part from the timestamp
        var hours = date.getHours();
        // Minutes part from the timestamp
        var minutes = "0" + date.getMinutes();
        // Seconds part from the timestamp
        var seconds = "0" + date.getSeconds();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        if (month < 10)
            month = "0" + month;
        var year = date.getFullYear();
        // Will display time in 10:30:23 format
        var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + " " + day + "/" + month + "/" + year;
        return formattedTime;
    }

    $scope.canEdit = function (username) {
        var model = sharedService.getUser();
        console.log(model);
        console.log(username);
        if (model.email === username) {
            return true;
        } 
        else
            return false;
    }

    $scope.delete = function (model) {
        console.log("Removing");
        backlog.push(model);
        var req = {
            method: 'PUT',
            url: $host + "/api/newsfeed/deletefeed",
            headers: {
                'Authorization': 'Bearer ' + sharedService.getToken()
            },
            params: {
                post_id : model.post_id
            }
        }
        $http(req).then(function (response) {
            console.log("removed");
            console.log(response);
            sharedService.hideFeed(model.post_id);
            for (var i = 0; i < backlog.length; i++) {
                if (backlog[i].post_id === model.post_id) {
                    backlog.splice(i, 1);
                    return true;
                }
            }
        }, function (response) {
            console.log("error");
            console.log(response);
            for (var i = 0; i < backlog.length; i++) {
                if (backlog[i].post_id === model.post_id) {
                    backlog.splice(i, 1);
                    return true;
                }
            }
        });
    }

    var backlog = new Array();

    $scope.isDeleting = function (model) {
        return backlog.indexOf(model) >= 0;
    }

    var myInterval;
    $scope.$on('$viewContentLoaded', function () {
        myInterval = setInterval(function () {
            sharedService.loadNewerFeeds();
        }, 3000);
        sharedService.loadNewerFeeds();
    });

    $scope.$on("$destroy", function () {
        clearInterval(myInterval);
    });
});
